# Article Knowledgebase Application

This is a knowledgebase app used in the "Node.js & Express From Scratch" Youtube series.

## Technologies
* Node.js
* Express
* Express Messages, Session, Connect Flash & Validation
* MongoDB & Mongoose
* Pug Templating
* Passport.js Authentication
* BCrypt Hashing

### Version
2.0.0

## Usage
This Platform For ADEF Tech Camp "T.O.T.O" To Share Creative Coding Work
### Installation

Install the dependencies

```sh
$ npm install
```
Run app

```sh
$ npm start
```

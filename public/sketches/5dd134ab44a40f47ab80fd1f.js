const a = (sketch) => {
  let canvas;
  sketch.setup = function(){
    // if you remove this line it won't work
    sketch.canvas = sketch.createCanvas(300, 300);
    sketch.canvas.parent('a');

  };

  sketch.draw = function(){
    // Set the background to black and turn off the fill color
    sketch.background(0, 0, 0);
  };
};
let myp51 = new p5(a);
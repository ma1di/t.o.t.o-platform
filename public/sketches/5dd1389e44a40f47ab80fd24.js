
const f = (sketch) => {
  let canvas;
  sketch.setup = function(){
    // if you remove this line it won't work
    sketch.canvas = sketch.createCanvas(300, 300);
    sketch.canvas.parent('f');

  };

  sketch.draw = function(){
    // Set the background to black and turn off the fill color
    sketch.background(255, 0, 0);
  };
};
let myp56 = new p5(f);